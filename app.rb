
require 'rubygems'
require 'bundler'

Bundler.require

$: << File.expand_path('../', __FILE__)

require 'app/routes'

module SinatraBootstrap
	class App < Sinatra::Application		
		configure do
			enable  :sessions
			set :session_secret, "laurelindorenan"
			disable :method_override
			disable :static
		end
		
		use Rack::Deflater
		use SinatraBootstrap::Routes::Client		
	end
end

