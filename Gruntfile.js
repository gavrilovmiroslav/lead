module.exports = function(grunt) {
 
    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
 
        watch: {
            scripts: {
                files: ['app/assets/javascripts/*.js'],
                options: {
                    spawn: false,
                },
            },
            css: {
                files: ['app/assets/stylesheets/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                }
            }
        },
        sass: {
            dist: {
                options: {
                    loadPath: require('node-bourbon').includePaths,
                    style: 'compressed'
                },
                files: {
                    'public/stylesheets/application.css': 'app/assets/stylesheets/application.scss'
                }
            }
        },
        autoprefixer: {
            dist: {
                files: {
                    'public/stylesheets/application.css': 'public/stylesheets/application.css'
                }
            }            
        }
    });
 
    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-watch');   // Watch those files and update accordingly
    grunt.loadNpmTasks('grunt-contrib-sass');    // Preprocess that CSS
    grunt.loadNpmTasks('grunt-autoprefixer');
 
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['sass', 'autoprefixer']);
 
};
