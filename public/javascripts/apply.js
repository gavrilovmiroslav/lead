
(function() {
	$(document).ready(function() {
		var countWords = function($e, $p) {
			return function() {
				var regex = /\s+/gi;
		 		var wordCount = $e.val().trim().replace(regex, ' ').split(' ').length;
		 		$p.text("Words: " + wordCount);
		 	}
		};

		var applicant_id = null;
		var applicant_info = null;

		var apply_details = function(callback) {
			$.post("/apply_details", { id : applicant_id }, function(dets) {
				applicant_info = JSON.parse(dets);
				
				if(applicant_info) {
					$("#info-step-name").val(applicant_info.data["info-step-name"] || "");
					$("#info-step-date").val(applicant_info.data["info-step-date"] || "");
					$("#info-step-mailing-address").val(applicant_info.data["info-step-mailing-address"] || "");
					$("#info-step-country").val(applicant_info.data["info-step-country"] || "");
					
					if(applicant_info.data["info-step-edu"])
						$("#info-step-edu option").filter(function() {
							return $(this).text() == applicant_info.data["info-step-edu"];
						}).prop('selected', true);

					$("#story-step-story").val(applicant_info.data["story-step-story"] || "");
					$("#why-step-story").val(applicant_info.data["why-step-story"] || "");
					$("#entrpr-step-story").val(applicant_info.data["entrpr-step-story"] || "");
					$("#learn-step-story").val(applicant_info.data["learn-step-story"] || "");
					$("#what-now-step-story").val(applicant_info.data["what-now-step-story"] || "");
				}

				$(".story").each(function(i, e) {
					void function($e, $p) {
						countWords($e, $p)();				
					}($(e), $(e).prev());
				});

				callback();
			});
		};

		var verify = function(id) {
			var verify_field = function(field) {
				var value = $(field).val();

				if($.trim(value) === "") {
					$(field)
						.animate({ backgroundColor : "red" })
						.delay(500)
						.animate({ backgroundColor : "white" });

					return false;
				}

				return value;
			}

			switch(id) {
				case "start-step":
					return function(callback) {
						var email = $("#start-step-email").val();
						var pass = $("#start-step-password").val();

						if($.trim(email) === "" || $.trim(pass) === "") {
							return;
						}

						$.post("/apply_start", { email : email, password : pass }, function(result) {
							if(result !== "" && result[0] !== "!") {
								applicant_id = result;
								apply_details(function() {
									callback(result);																		
								});
							} else {
								if(result === "!incorrect password") {
									$(".subl")
										.text("Error: Incorrect password!")
										.animate({ color: "red" })
										.delay(300)
										.animate({ color : "gray" });

									setTimeout(function() {										
										$(".subl").text("The password you enter now will be saved in case you still don't have an account.");										
									}, 2000);
								}
							}
						});
					}
				case "info-step":
					return function(callback) {
						var name = verify_field("#info-step-name");
						var date = verify_field("#info-step-date");
						var address = verify_field("#info-step-mailing-address");
						var country = verify_field("#info-step-country");					
						var edu = $("#info-step-edu option:selected").text();

						if(name && date && address && country && edu) {
							$.post("/apply_save", 
							{
								"id" : applicant_id, 
								"info-step-name" : name, 
								"info-step-date" : date,
								"info-step-mailing-address" : address,
								"info-step-country" : country,
								"info-step-edu" : edu
							}, function() {
								callback();
							});
						}
					}
				default:
					return function(callback) {
						var story = verify_field("#" + id + "-story");
						if(story) {
							var ob = { "id" : applicant_id };
							ob[id + "-story"] = story;
							ob["done"] = (id === "what-now-step");
							$.post("/apply_save", ob, 
							function() {
								if(ob["done"]) {
									$.post("/send_thanks", { mail: $("#start-step-email").val() }, function() {
										console.log("mail sent!");
									});
								}
								callback();
							});
						}
					}				
			}
		}

		$(".step").hide();
		$(".step:first-child").show();
		$(".step .next").click(function() {
			var $pp = $(this).parent().parent().parent(); 
			verify($pp.attr("id"))(function() {
				$pp.fadeOut(function() {
					$pp.next().fadeIn();
				});
			});
		});

		$(".step .back").click(function() {
			var $pp = $(this).parent().parent().parent();
			$pp.fadeOut(function() {
				$pp.prev().fadeIn();
			});
		});

		$(".story").each(function(i, e) {
			void function($e, $p) {
				$e.keydown(countWords($e, $p));				
			}($(e), $(e).prev());
		});

		$.post("/apply_continue", {}, function(id) {
			if(id !== "false") {
				applicant_id = id;
				apply_details(function() {
					if(applicant_info && applicant_info.data && applicant_info.data.done) {
						$("#start-step").fadeOut(function() {
							$("#already-in").fadeIn();
						})
					} else {
						$("#start-step").fadeOut(function() {
							$("#start-step").next().fadeIn();
						});				
					}
				});
			}
		});
	});
})();