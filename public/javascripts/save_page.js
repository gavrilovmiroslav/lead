
void function() {
	function utf8_to_b64( str ) {
	  return window.btoa(unescape(encodeURIComponent(str)));
	}

	function b64_to_utf8( str ) {
	  return decodeURIComponent(escape(window.atob(str)));
	}

	$(function() {
		$("#save-button").click(function(e) {
			var html = $("#editor").html();

			var contents = utf8_to_b64(html);

			$.post("/save_page", 
				{ 
					page : $("#page").attr("src"), 
					contents : contents 
				}, 

			function(result) {
				window.location.href = window.location.href.replace("/change", "");
			});

			e.preventDefault();
			return false;
		});
	});
}();