$(function() {
    window.onbeforeunload = function() {}

    var editor = document.getElementById("editor"), pen;
    if(editor !== null) {
        var options = {
            editor : editor,
            class: 'pen',
            debug: false,
            textarea: '<textarea name="content"></textarea>',
            list: [
                'bold', 'italic', 'underline', 
                'h2', 'h3', 'p', 'createlink', 
                'insertorderedlist', 'insertunorderedlist'
            ],
            stay: true
        };

        pen = new Pen(options);
    }

    $(document).ready(function() {
        var html = "<div class='columnize'>" + 
            "<a href='/programs/exl-dartmouth'>ExL at Dartmouth</a>" + 
            "<a href='/programs/exl-rvcc'>ExL at RVCC</a>" + 
            "<a href='/programs/balkan'>Balkan Entrepreneurship Winter Program</a>" + 
        "</div>";

        var toggled = false;

        var callback = function(ev) {
            if(toggled) {
                ev.preventDefault();
                return false;
            }
            
            toggled = true;

            var $html = $(html);
            
            $html.on('mouseleave', function() {
                $("#programs-button").toggleClass("push-down");
                $html.remove();

                toggled = false;
            });

            $(this).append($html);
            $(this).toggleClass("push-down");

            ev.preventDefault();
            return false;
        }

        $("#programs-button").on('click', callback);
    });
});