
module SinatraBootstrap
	module Routes
		autoload :Base, 'app/routes/base'
		autoload :Client, 'app/routes/client'		
	end
end
