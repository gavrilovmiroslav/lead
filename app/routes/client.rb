
require 'rethinkdb'
require 'digest'

include RethinkDB::Shortcuts

r.connect(:host => "localhost", :port => 28015).repl

module SinatraBootstrap
	module Routes
		class Client < Base
      
      # logout

      get "/logout" do
        session[:user] = nil

        redirect "/"
      end

      post "/send_thanks" do
        mail = params[:mail]
        return if mail.nil?

        RestClient.post "https://api:key-566c9af9a9c41cf0df05c58d65fa4e63"\
          "@api.mailgun.net/v2/sandboxe694430aa6b247679f4c25edf55efdd1.mailgun.org/messages",
          :from => "LEaD Summer Program <lead@exluniversity.com>",
          :to => mail,
          :subject => "Application submission confirmed!",
          :html => File.read("app/views/mail.html")        
      end

      # apply now!

      post "/apply_details" do
        id = params[:id]

        r.db("lead").table("applicants").get(id).run.to_json
      end

      post "/apply_save" do
        id = params[:id]
        params.delete "id"
        params.delete :id
        puts r.db("lead").table("applicants").get(id).update({ :data => params }).run
      end

      post "/apply_continue" do
        if session[:participant].nil?
          return "false"
        else 
          return session[:participant]
        end
      end

      post "/apply_start" do
        mail = params[:email]
        password = Digest::SHA2.hexdigest params[:password]

        cursor = r.db("lead").table("applicants").filter { |applicant|
          applicant["mail"].eq(mail)
        }.run.to_a

        if cursor.length > 0
          if cursor[0]["password"] == password
            session[:participant] = cursor[0]["id"]
            return cursor[0]["id"]
          else
            return "!incorrect password"
          end
        else
          res = r.db("lead").table("applicants").insert({ :mail => mail, :password => password, :data => {} }).run
          session[:participant] = res["generated_keys"][0]
          return res["generated_keys"][0]
        end
      end

      post "/save_page" do
        @page = params[:page]
        @contents = params[:contents]

        data = { :id => @page, :contents => @contents }

        r.db("lead").table("pages").insert(data, :conflict => "replace").run
        haml :index
      end

      # login 

      post "/attempt_login" do
        @username = params[:username]
        @password = Digest::SHA2.hexdigest params[:password]

        cursor = r.db("lead").table("users").filter { |user| 
          user["mail"].eq(@username) and user["password"].eq(@password) 
        }.run.to_a

        if cursor.length > 0
          session[:user] = cursor[0]
          redirect "/"
        else
          redirect "/login"
        end
      end

      # special 

      [ "login", "apply" ].each do |key|
        page_name = key
        page_name = "index" if key == "" 

        get "/#{key}" do
          haml "/#{page_name}".intern
        end
      end

      # ordinary pages

      [ "", "index", "about", "admission",
        "faq", "contact",
        "faculty", "curriculum",
        "programs/lead",
        "programs/exl-dartmouth",
        "programs/exl-rvcc",
        "programs/balkan" ].each do |key|

        page_name = key
        page_name = "index" if key == "" 

     	  get "/#{key}" do
          @changing = nil
          @page_name = page_name

          @data = r.db('lead').table('pages').get(page_name).run
          
          if @data.nil?
            haml "/#{page_name}".intern
          else
            require 'base64'
            @contents = @data["contents"]
            @contents = Base64.decode64("#{@contents}\n").force_encoding('UTF-8').encode
            haml :page
          end
     	  end

        get "/#{key}/change" do
          if session[:user].nil?
            redirect "/#{key}"
            return
          end

          @changing = true
          @page_name = page_name

          @data = r.db('lead').table('pages').get(page_name).run
          
          if @data.nil?
            haml "/#{page_name}".intern
          else
            require 'base64'
            @contents = @data["contents"]
            @contents = Base64.decode64("#{@contents}\n").force_encoding('UTF-8').encode
            haml :page
          end
        end
     	end
		end
	end
end